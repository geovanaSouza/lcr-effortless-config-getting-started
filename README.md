# Overview
This repo contains the base material for the 'Managing a Node With Policyfiles' Learn Chef Rally module.

# Pre-requisites

* Chef Workstation
* Docker

# Instructions

* Create an account on Chef Habitat Origin: https://bldr.habitat.sh/#/origins
* Copy the keys generated on your account in `https://bldr.habitat.sh/#/origins/<your_account_here>/keys` to ~/.hab/cache/keys in your machine
* Configure your machine using the instruction [here](https://docs.chef.io/habitat/hab_setup/), You can skip the configuration about the Supervisor control gateway
* Set the HAB_ORIGIN environment variable with your origin name created on [Chef Habitat Origin](https://bldr.habitat.sh/#/origins)

# How to initialize the Habitat

```bash
hab plan init
```

# Files

* habitat/plan.sh - File with instructions about how we build, deploy and manage our application. This file has information like as the policy that will be installed on targets, the name of the package, the instructions that will be used to build to Habitat Package, environments that will be shared with the Supervisor.
* habitat/default.toml - In our build, we will package the Chef Infra Client Instalation. This file has a content with some parameters that is required to configure the Chef Infra Client.
* bootstrap.sh - The file used by vagrant to provisioner the environment

# Build the Habitat Package

```bash
hab studio enter
build
exit
```

The package will be generated on results/ directory

# Testing the habitat package with kitchen

```bash
export HAB_ORIGIN=<your_habitat_origin_name_here>
kitchen converge
```

Make sure if the apply run with success

```bash
kitchen verify
```

# Upload the package to Chef Habitat Builder

```bash
export SSL_CERT_FILE=/opt/chef-workstation/embedded/ssl/cert.pem
source results/last_build.env
hab pkg upload results/$pkg_artifact
```

## Promote package to stable

```bash
hab pkg promote $pkg_ident stable
```

Check the published package on https://bldr.habitat.sh/#/origins
